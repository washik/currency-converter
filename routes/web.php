<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Auth::routes();

// Get route om alleen convertcurrency.blade.php te laten zien met de currency lijst
Route::get('/convertcurrency', 'ConvertsController@index')->name('converter');
// Post route om de gevraagde currency om te zetten met de convertCurrency method
Route::post('/convertcurrency', 'ConvertsController@convertCurrency')->name('currency');
