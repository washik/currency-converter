<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Cache;

use Session;

class ConvertsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Alleen toegang voor gebruikers die zijn ingelogd
        $this->middleware('auth');
    }

    // Helper method om currency lijst op te halen
    protected function getCurrencies() {
        // Currency lijst ophalen van API
        $json3 = file_get_contents("https://free.currencyconverterapi.com/api/v6/currencies");
        // Resulaten in een array zetten
        $currencies = json_decode($json3, true);
        // Array duidelijker maken om extra foreach loop te voorkomen
        $currencies = array_shift($currencies);
        // Array alfabetisch sorteren
        ksort($currencies);
        return $currencies;
    }


    // Index method die convertcurrency.blade.php returned met de currency lijst
    public function index() {
        // Currency list voor 1 dag cachen om webservice niet te belasten, geld te besparen en snelheid te verbeteren
        $currencies = Cache::remember('currencies', 1440, function() { 
            return ConvertsController::getCurrencies();
        });
        return view('convertcurrency', ['currencies' => $currencies]);
    }

    public function convertCurrency() {
        // Currency list voor 1 dag cachen om webservice niet te belasten, geld te besparen en snelheid te verbeteren
        $currencies = Cache::remember('currencies', 1440, function() { 
            return ConvertsController::getCurrencies();
        });
        // De info die nodig is van de gebruiker in variables zetten
        $amount = Input::get('amount');
        $from_currency = Input::get('from_currency');
        $to_currency = Input::get('to_currency');

        // Query maken voor de webservice
        $query = "{$from_currency}_{$to_currency}";
        // Currency rate ophalen die de gebruiker heeft ingevoerd
        $json = file_get_contents("https://free.currencyconverterapi.com/api/v6/convert?q={$query}&compact=ultra");
        // Float maken van de gevraagde currency rate
        $obj = json_decode($json, true);
        $val = floatval($obj["$query"]);
        // Totaal berekenen van gevraagde currency
        $total = $val * $amount;

        // Query maken voor $opposite_rate
        $oquery = "{$to_currency}_{$from_currency}";
        // Opposit currency rate ophalen
        $json2 = file_get_contents("https://free.currencyconverterapi.com/api/v6/convert?q={$oquery}&compact=ultra");
        // Float maken van opposite currency rate
        $obj2 = json_decode($json2, true);
        $opposite_rate = floatval($obj2["$oquery"]);

        // Return convertcurrency.blade.php met alle benodigde variabelen
        return view('convertcurrency', [
            'total' => $total,
            'amount' => $amount,
            'from_currency' => $from_currency,
            'to_currency' => $to_currency,
            'currencies' => $currencies,
            'val' => $val,
            'opposite_rate' => $opposite_rate
            ]);
    }
}