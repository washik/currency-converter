# CurCon

###Currency converter

Voor dit project heb ik gekozen om Laravel te gebruiken voor de gemakkelijke en veilige implementatie van een login systeem en het csrf veld voor een veilig formulier. Het inbrengen van eigen code of webservices is ook geen probleem.

Voor de setup van dit project op een locale omgeving met een MySQL databse, moet u de volgende elementen in het .env-example bestand aanpassen:

- DB_DATABASE=homestead     -> vervang "homestead" met uw database naam
- DB_USERNAME=homestead     -> vervang "homestead" met uw database username
- DB_PASSWORD=secret        -> vervang "secret" met uw database wachtwoord

Hierna moet u alleen nog het bestand hernoemen en ".example" verwijderen uit de naam zodat alleen ".env" overblijft.