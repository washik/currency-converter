@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <!-- Begin panel voor de converter -->
        <div class="panel panel-default">
            <!-- Panel head -->
            <div class="panel-heading">
                Currency Converter
            </div>
            <!-- Panel body -->
            <div class="panel-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <!-- Begin form voor user input en resultaten -->
                <form method="POST" action="{{ route('currency') }}">
                    <!-- Begin row voor select boxes -->
                    <div class="form-group row">
                        <div class="col-md-6">
                            Currency I have:<br>
                            <select name="from_currency" class="form-control">
                                <!-- Foreach loop om currencies array als lijst te tonen -->
                                @foreach($currencies as $currency)
                                    <option value="{{ $currency['id'] }}">
                                        {{ $currency['id'] }} | {{ $currency['currencyName'] }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            Currency I want:<br>
                            <select name="to_currency" class="form-control">
                                <!-- Foreach loop om currencies array als lijst te tonen -->
                                @foreach($currencies as $currency)
                                    <option value="{{ $currency['id'] }}">
                                            {{ $currency['id'] }} | {{ $currency['currencyName'] }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <!-- Einde row voor select boxes -->
                    <!-- Begin row voor amounts -->
                    <div class="row">
                        <div class="col-md-6">
                            <input class="form-control" type="number" min="0.01" step="0.01" name="amount" placeholder="Amount I have">
                        </div>
                        <div class="col-md-6">
                                Amount
                            <div class="well"> 
                                @isset($total, $to_currency) <h4>{{ $total }}</h4> &nbsp;&nbsp;{{ $to_currency }} @endisset
                            </div>
                        </div><br>
                    </div>
                    <!-- Einde row voor amounts -->
                    <!-- Row met currency rate en opposite rate -->
                    <div class="form-group row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            Current Rate
                            <div class="well well-lg">
                                @isset($from_currency, $to_currency, $val, $opposite_rate)
                                    <h4> {{ $from_currency }} / {{ $to_currency }} : </h4> &nbsp;&nbsp; {{ $val }} &nbsp;&nbsp; <h6> as on {{ date('d-m-Y H:i:s') }} </h6> <br>
                                    <h4> {{ $to_currency }} / {{ $from_currency }} : </h4> &nbsp;&nbsp; {{ $opposite_rate }} &nbsp;&nbsp; <h6> as on {{ date('d-m-Y H:i:s') }} </h6>
                                @endisset
                            </div>
                        </div>
                    </div>
                    <!-- Einde row currency rate en opposite rate -->
                    <div class="row text-center">
                    <button type="submit" class="btn btn-lg btn-success"><i class="fas fa-exchange-alt"></i></button>
                    <button class="btn btn-lg btn-danger" type="reset"><i class="fas fa-undo"></i></button>
                    </div>
                    {{ csrf_field() }}
                </form>
                <!-- Einde form voor user input en resultaten -->
            </div>
            <!-- Einde panel body -->
        </div>
        <!-- Einde panel van de converter -->
    </div>
</div>
@endsection
