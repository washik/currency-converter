@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h3>Welcome to CurCon!</h3>
                    <hr>
                    <p><h4>We are offering a fast, reliable and responsive web app to convert one currency to another for a small amount per month.</h4></p>
                    <br>
                    @guest
                    <p><h6>To use our converter you must be logged in. If you do not have an account, please register first.</h6></p>
                    @else
                    <a class="btn btn-info" href="convertcurrency">Convert Currency</a>
                    @endguest
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
